/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mck.service.dwmytest.web.rest.vm;
