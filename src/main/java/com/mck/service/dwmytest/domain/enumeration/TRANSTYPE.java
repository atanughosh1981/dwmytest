package com.mck.service.dwmytest.domain.enumeration;

/**
 * The TRANSTYPE enumeration.
 */
public enum TRANSTYPE {
    DEBIT, CREDIT
}
