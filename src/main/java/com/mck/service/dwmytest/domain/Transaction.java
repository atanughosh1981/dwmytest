package com.mck.service.dwmytest.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import com.mck.service.dwmytest.domain.enumeration.TRANSTYPE;

/**
 * A Transaction.
 */
@Entity
@Table(name = "transaction")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Transaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "account_id", nullable = false)
    private Long accountId;

    @NotNull
    @Column(name = "transaction_date", nullable = false)
    private LocalDate transactionDate;

    @NotNull
    @Column(name = "transaction_remark", nullable = false)
    private String transactionRemark;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "transaction_type", nullable = false)
    private TRANSTYPE transactionType;

    @NotNull
    @Column(name = "available_balance", nullable = false)
    private Double availableBalance;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccountId() {
        return accountId;
    }

    public Transaction accountId(Long accountId) {
        this.accountId = accountId;
        return this;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public LocalDate getTransactionDate() {
        return transactionDate;
    }

    public Transaction transactionDate(LocalDate transactionDate) {
        this.transactionDate = transactionDate;
        return this;
    }

    public void setTransactionDate(LocalDate transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionRemark() {
        return transactionRemark;
    }

    public Transaction transactionRemark(String transactionRemark) {
        this.transactionRemark = transactionRemark;
        return this;
    }

    public void setTransactionRemark(String transactionRemark) {
        this.transactionRemark = transactionRemark;
    }

    public TRANSTYPE getTransactionType() {
        return transactionType;
    }

    public Transaction transactionType(TRANSTYPE transactionType) {
        this.transactionType = transactionType;
        return this;
    }

    public void setTransactionType(TRANSTYPE transactionType) {
        this.transactionType = transactionType;
    }

    public Double getAvailableBalance() {
        return availableBalance;
    }

    public Transaction availableBalance(Double availableBalance) {
        this.availableBalance = availableBalance;
        return this;
    }

    public void setAvailableBalance(Double availableBalance) {
        this.availableBalance = availableBalance;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Transaction transaction = (Transaction) o;
        if (transaction.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), transaction.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Transaction{" +
            "id=" + getId() +
            ", accountId=" + getAccountId() +
            ", transactionDate='" + getTransactionDate() + "'" +
            ", transactionRemark='" + getTransactionRemark() + "'" +
            ", transactionType='" + getTransactionType() + "'" +
            ", availableBalance=" + getAvailableBalance() +
            "}";
    }
}
